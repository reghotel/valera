﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Client
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public int money { get; set; }

        public Client()
        {

        }
        public Client(string fname, string lname, string login, string password, int money)
        {
            this.fname = fname;
            this.lname = lname;
            this.login = login;
            this.password = password;
            this.money = money;
        }
        public void registration(string fname, string lname, string login, string password, int money)
        {
            Data.clientlist.Add(new Client(fname, lname, login, password, money));
        }


        public bool vlogin(string login, string password)
        {
            foreach (var client in Data.clientlist)
            {
                if (login == client.login && password == client.password)
                {
                    CurrentUser.getInstance().currentclient = client;
                    return true;
                }
            }
            return false;
            
        }
    }
}
