﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class CreateHotel : Form
    {
        public CreateHotel()
        {
            InitializeComponent();
        }

        private void CreateHotel_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
                {
                    MessageBox.Show("Заповніть всі поля");
                }
                else
                {
                    try
                    {
                        string namehotel = textBox1.Text;
                        string streetH = textBox2.Text;
                        string place = textBox3.Text;
                        int places = Int32.Parse(place);
                        string pric = textBox4.Text;
                        int price = Int32.Parse(pric);
                        string hpric = textBox5.Text;
                        int hprice = Int32.Parse(hpric);
                        Hotel.addhotel(namehotel, streetH, places, price, hprice);
                        Home home = new Home();
                        this.Hide();
                        home.Show();
                    }catch(OverflowException exp)
                    {
                        MessageBox.Show("Ніфіга");
                    }
                }
            }
            catch (FormatException exp)
            {
                MessageBox.Show("Невірне значення");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Home home =new Home();
            this.Hide();
            home.Show();
        }
    }
}
