﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class EditProfile : Form
    {
        public EditProfile()
        {
            InitializeComponent();
        }

        private void EditProfile_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
            {
                MessageBox.Show("Заповніть всі поля");
            }
            else
            {
                string fname = textBox1.Text;
                CurrentUser.getInstance().currentclient.fname = fname;
                string lname = textBox2.Text;
                CurrentUser.getInstance().currentclient.lname = lname;
                string login = textBox3.Text;
                CurrentUser.getInstance().currentclient.login = login;
                string password = textBox4.Text;
                string checkpass = textBox5.Text;
                if(password==checkpass)
                {
                    CurrentUser.getInstance().currentclient.password = password;
                    Home home = new Home();
                    home.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Паролі не збігаються");
                }  
                
            }
        }

        private void textBox1_TextChanged(object sender,EventArgs e)
        {
            if (Regex.IsMatch(textBox1.Text, @"\d+"))
            {
                textBox1.ForeColor = Color.Red;
                MessageBox.Show("Невірне ім'я");
            }
            else
            {
                textBox1.ForeColor = Color.Black;
            }
        }
    }
}

