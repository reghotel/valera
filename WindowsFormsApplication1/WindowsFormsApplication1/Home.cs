﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
            label6.Text = CurrentUser.getInstance().currentclient.fname.ToString();
            label7.Text = CurrentUser.getInstance().currentclient.lname.ToString();
            label8.Text = CurrentUser.getInstance().currentclient.login.ToString();
            int money = CurrentUser.getInstance().currentclient.money;
            string sMoney = money.ToString();
            label9.Text = sMoney;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SearchedHotel search = new SearchedHotel();
            this.Hide();
            search.Show();

        }

        public void label6_Click(object sender, EventArgs e)
        {

        }


        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string namehotel = textBox1.Text;
                bool ifname = Hotel.searchwithname(namehotel);
                if (ifname)
                {
                    foreach (var hotel in Data.hotellist)
                    {
                        if (namehotel == hotel.nameHotel)
                        {
                            if (hotel.places > 0 && CurrentUser.getInstance().currentclient.money >= hotel.price)
                            {
                                CurrentUser.getInstance().currentclient.money -= hotel.price;
                                hotel.places -= 1;
                                Hotel.Reserv(hotel);
                                break;
                            }
                            else if (hotel.places <= 0)
                            {
                                MessageBox.Show("Відсутні місця");
                            }
                            else if (CurrentUser.getInstance().currentclient.money <= hotel.price)
                            {
                                MessageBox.Show("Недостатньо коштів");
                            }
                        }
                    }
                }


                else
                {
                    MessageBox.Show("Невірний ввід");
                }

                listView1.Clear();
                listView1.Visible = true;
                listView1.View = View.Details;
                listView1.GridLines = true;
                listView1.FullRowSelect = true;
                listView1.Columns.Add("Name Hotel", 100);
                listView1.Columns.Add("Hotel adress", 100);
                listView1.Columns.Add("Room count", 75);
                listView1.Columns.Add("Room price", 75);

                foreach (var hotel in Data.roomlist)
                {
                    ListViewItem itm;
                    String[] hotellist = new String[6];
                    hotellist[0] = hotel.nameHotel.ToString();
                    hotellist[1] = hotel.streetH.ToString();
                    hotellist[2] = hotel.places.ToString();
                    hotellist[3] = hotel.price.ToString();
                    itm = new ListViewItem(hotellist);
                    listView1.Items.Add(itm);
                }
            }
            catch (NullReferenceException exp)
            {
                foreach (var hotel in Data.roomlist)
                {
                    ListViewItem itm;
                    String[] hotellist = new String[6];
                    hotellist[0] = hotel.nameHotel.ToString();
                    hotellist[1] = hotel.streetH.ToString();
                    hotellist[2] = hotel.places.ToString();
                    hotellist[3] = hotel.price.ToString();
                    itm = new ListViewItem(hotellist);
                    listView1.Items.Add(itm);
                }
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            CreateHotel add = new CreateHotel();
            this.Hide();
            add.Show();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            string namehotel = textBox2.Text;
            bool ifname = Hotel.searchwithname(namehotel);
            if (ifname)
            {
                foreach (var hotel in Data.hotellist)
                {
                    if (namehotel == hotel.nameHotel)
                    {
                        if (hotel.owner == null)
                        {
                            hotel.owner = CurrentUser.getInstance().currentclient;
                            CurrentUser.getInstance().currentclient.money -= hotel.hprice;
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Готель має власника");
                        }
                    }
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            CurrentUser.getInstance().currentclient = null;
            Login login = new Login();
            this.Hide();
            login.Show();
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            EditProfile edit = new EditProfile();
            this.Hide();
            edit.Show();
        }
    }
}

