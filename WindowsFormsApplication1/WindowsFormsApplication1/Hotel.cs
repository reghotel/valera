﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Hotel
    {
        public string nameHotel { get; set; }
        public string streetH { get; set; }
        public Client owner { get; set; }
        public int places { get; set; }
        public int price { get; set; }
        public int hprice { get; set; }

        public Hotel()
        {

        }
        public Hotel(Hotel hotel)
        {
            this.nameHotel = hotel.nameHotel;
            this.streetH = hotel.streetH;
            this.places = hotel.places;
            this.price = hotel.price;
            this.owner = owner;

        }
        public Hotel(string nameHotel, string streetH, int places, int price, int hprice)
        {
            this.nameHotel = nameHotel;
            this.streetH = streetH;
            this.places = places;
            this.price = price;
            this.hprice = hprice;
            
        }
        public Hotel(string nameHotel, string streetH, int places, int price, int hprice,Client owner)
        {
            this.nameHotel = nameHotel;
            this.streetH = streetH;
            this.places = places;
            this.price = price;
            this.hprice = hprice;
            this.owner = owner;
        }
        public static bool searchwithname(string nameHotel)
        {

            foreach (var Hotel in Data.hotellist)
            {
                if (nameHotel == Hotel.nameHotel)
                {
                    return true;
                }
            }
            return false;
        }
        public static void Reserv(Hotel hotel)
        {
            Data.roomlist.Add(new Hotel(hotel));
        }
        public static bool searchwithstreetH(string streetH)
        {
            foreach (var Hotel in Data.hotellist)
            {
                if (streetH == Hotel.streetH)
                {
                    return true;
                }
            }
            return false;
        }
        public static bool searchwithprice(int price)
        {
            foreach (var Hotel in Data.hotellist)
            {
                if (price >= Hotel.price)
                {
                    return true;
                }
            }
            return false;
        }
        public static void addhotel(string namehotel,string streetH, int places, int price,int hprice)
        {
            Data.hotellist.Add(new Hotel(namehotel, streetH, places, price, hprice));
        }
        public static void buyhotel(Client owner)
        {
            
        }
    }
}
