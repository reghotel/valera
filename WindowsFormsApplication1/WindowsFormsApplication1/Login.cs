﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
      

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            Client islogin = new Client();
            string login = Vlogin.Text;
            string password = Vpass.Text;
            bool enterlogin = islogin.vlogin(login, password);
            if (enterlogin)
            {
                Home home = new Home();
                this.Hide();
                home.Show();
                Data.hotellist.Add(new Hotel() { nameHotel = "Galichina", streetH = "Ruska,56", places = 25, price = 100, hprice = 10000 });
                Data.hotellist.Add(new Hotel() { nameHotel = "Palitechnik", streetH = "Centrova,1", places = 45, price = 228, hprice = 110000 });
                Data.hotellist.Add(new Hotel() { nameHotel = "EconomicByrsa", streetH = "Vasulkivska,1", places = 10, price = 300, hprice = 12000 });
                Data.hotellist.Add(new Hotel() { nameHotel = "MedByrsa", streetH = "Lychakivskogo,4", places = 98, price = 1488, hprice = 1000 });
                Data.hotellist.Add(new Hotel() { nameHotel = "PedByrsa", streetH = "Hohola,7", places = 45, price = 322, hprice = 14400 });
            }
            else
            {
                Vlogin.Clear();
                Vpass.Clear();
                MessageBox.Show("Невірний логін або пароль");
            }
            
        }
 
        private void button1_Click(object sender, EventArgs e)
        {
            Registration newForm = new Registration();
            this.Hide();
            newForm.Show();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Vlogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void Vpass_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Hide();
        }
    }
}
