﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
                {
                    MessageBox.Show("Заповніть всі поля");
                }
                else
                {
                    try
                    {
                    Client client = new Client();
                    string fname = textBox1.Text;
                    string lname = textBox2.Text;
                    string login = textBox3.Text;
                    string password = textBox4.Text;
                    string hmoney = textBox5.Text;
                    Regex regex = new Regex(@"^[0-9]+$|\S");
                    int money = Int32.Parse(hmoney);
                        foreach(var l in Data.clientlist)
                        {
                            if(login==l.login)
                            {
                                MessageBox.Show("Логін зайнятий");
                            }
                        }
                    client.registration(fname, lname, login, password, money);
                    }
                    catch(OverflowException exp)
                    {
                        MessageBox.Show("А ніхуя Валера");
                    }        
                    
                    Login login1 = new Login();
                    login1.Show();
                    this.Hide();

                }
            }
            catch (FormatException exp)
            {
                MessageBox.Show("Невірне значення");
            }

    }

    private void button2_Click(object sender, EventArgs e)
    {
        Login login = new Login();
        login.Show();
        this.Hide();
    }

    private void Registration_Load(object sender, EventArgs e)
    {

    }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(textBox1.Text, @"\d+"))
            {
                textBox1.ForeColor = Color.Red;
                MessageBox.Show("Невірне ввід");
            }
            else
            {
                textBox1.ForeColor = Color.Black;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(textBox1.Text, @"\d+"))
            {
                textBox1.ForeColor = Color.Red;
                MessageBox.Show("Невірний ввід");
            }
            else
            {
                textBox1.ForeColor = Color.Black;
            }
        }
    }
}
