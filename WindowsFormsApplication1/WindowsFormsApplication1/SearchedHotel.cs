﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class SearchedHotel : Form
    {


        public SearchedHotel()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            ListHotel.Clear();
        }

        private void poshukBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ListHotel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ListHotel.Clear();
            string namehotel = textBox1.Text;
            bool ifname = Hotel.searchwithname(namehotel);
            if (ifname)
            {
                ListHotel.Clear();
                ListHotel.Visible = true;
                ListHotel.View = View.Details;
                ListHotel.GridLines = true;
                ListHotel.FullRowSelect = true;
                ListHotel.Columns.Add("Name Hotel", 100);
                ListHotel.Columns.Add("Hotel adress", 100);
                ListHotel.Columns.Add("Room count", 75);
                ListHotel.Columns.Add("Room price", 75);
                ListHotel.Columns.Add("Hotel owner", 75);
                ListHotel.Columns.Add("Last name", 75);
                try
                {
                    foreach (var hotel in Data.hotellist)
                    {
                        if (hotel.nameHotel == namehotel)
                        {
                            ListViewItem itm;
                            String[] hotellist = new String[6];
                            hotellist[0] = hotel.nameHotel.ToString();
                            hotellist[1] = hotel.streetH.ToString();
                            hotellist[2] = hotel.places.ToString();
                            hotellist[3] = hotel.price.ToString();
                            hotellist[4] = hotel.owner.fname.ToString();
                            hotellist[5] = hotel.owner.lname.ToString();

                            itm = new ListViewItem(hotellist);
                            ListHotel.Items.Add(itm);
                        }
                    }
                }
                catch (NullReferenceException exp)
                {
                    foreach (var hotel in Data.hotellist)
                    {
                        if (hotel.nameHotel == namehotel)
                        {
                            ListViewItem itm;
                            String[] hotellist = new String[6];
                            hotellist[0] = hotel.nameHotel.ToString();
                            hotellist[1] = hotel.streetH.ToString();
                            hotellist[2] = hotel.places.ToString();
                            hotellist[3] = hotel.price.ToString();
                            hotellist[4] = "none";
                            hotellist[5] = "none";

                            itm = new ListViewItem(hotellist);
                            ListHotel.Items.Add(itm);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Невірний ввід");
            }   
        }

        private void SearchedHotel_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ListHotel.Clear();
            string streetH = textBox1.Text;
            bool ifStreet = Hotel.searchwithstreetH(streetH);
            if (ifStreet)
            {
                ListHotel.Visible = true;
                ListHotel.View = View.Details;
                ListHotel.GridLines = true;
                ListHotel.FullRowSelect = true;
                ListHotel.Columns.Add("Name Hotel", 100);
                ListHotel.Columns.Add("Hotel adress", 100);
                ListHotel.Columns.Add("Room count", 75);
                ListHotel.Columns.Add("Room price", 75);
                ListHotel.Columns.Add("Hotel owner", 75);
                ListHotel.Columns.Add("Last name", 75);
                try
                {
                    foreach (var hotel in Data.hotellist)
                    {
                        if (hotel.streetH == streetH)
                        {
                            ListViewItem itm;
                            String[] hotellist = new String[4];
                            hotellist[0] = hotel.nameHotel.ToString();
                            hotellist[1] = hotel.streetH.ToString();
                            hotellist[2] = hotel.places.ToString();
                            hotellist[3] = hotel.price.ToString();
                            hotellist[4] = hotel.owner.fname.ToString();
                            hotellist[5] = hotel.owner.lname.ToString();
                            itm = new ListViewItem(hotellist);
                            ListHotel.Items.Add(itm);
                        }
                    }
                }
                catch (NullReferenceException exp)
                {
                    foreach (var hotel in Data.hotellist)
                    {
                        if (hotel.streetH == streetH)
                        {
                            ListViewItem itm;
                            String[] hotellist = new String[6];
                            hotellist[0] = hotel.nameHotel.ToString();
                            hotellist[1] = hotel.streetH.ToString();
                            hotellist[2] = hotel.places.ToString();
                            hotellist[3] = hotel.price.ToString();
                            hotellist[4] = "none";
                            hotellist[5] = "none";

                            itm = new ListViewItem(hotellist);
                            ListHotel.Items.Add(itm);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Невірний ввід");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ListHotel.Clear();
            try
            {
                string sPrice = textBox1.Text;
                int iPrice = int.Parse(sPrice);

                bool ifprice = Hotel.searchwithprice(iPrice);
                if (ifprice)
                {
                    ListHotel.Visible = true;
                    ListHotel.View = View.Details;
                    ListHotel.GridLines = true;
                    ListHotel.FullRowSelect = true;
                    ListHotel.Columns.Add("Name Hotel", 100);
                    ListHotel.Columns.Add("Hotel adress", 100);
                    ListHotel.Columns.Add("Room count", 75);
                    ListHotel.Columns.Add("Room price", 75);
                    ListHotel.Columns.Add("Hotel owner", 75);
                    ListHotel.Columns.Add("Last name", 75);
                    try
                    {
                        foreach (var hotel in Data.hotellist)
                        {
                            if (hotel.price <= iPrice)
                            {
                                ListViewItem itm;
                                String[] hotellist = new String[4];
                                hotellist[0] = hotel.nameHotel.ToString();
                                hotellist[1] = hotel.streetH.ToString();
                                hotellist[2] = hotel.places.ToString();
                                hotellist[3] = hotel.price.ToString();
                                hotellist[4] = hotel.owner.fname.ToString();
                                hotellist[5] = hotel.owner.lname.ToString();
                                itm = new ListViewItem(hotellist);
                                ListHotel.Items.Add(itm);
                            }
                        }
                    }
                    catch (NullReferenceException exp)
                    {
                        foreach (var hotel in Data.hotellist)
                        {
                            if (hotel.price <= iPrice)
                            {
                                ListViewItem itm;
                                String[] hotellist = new String[6];
                                hotellist[0] = hotel.nameHotel.ToString();
                                hotellist[1] = hotel.streetH.ToString();
                                hotellist[2] = hotel.places.ToString();
                                hotellist[3] = hotel.price.ToString();
                                hotellist[4] = "none";
                                hotellist[5] = "none";

                                itm = new ListViewItem(hotellist);
                                ListHotel.Items.Add(itm);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Невірний ввід");
                }
            }
            catch
            {
                MessageBox.Show("А ніхуя");
            }
        }

    private void button6_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            this.Hide();
            home.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }
    }
}
